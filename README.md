# utlpracticeaugust24th

Created a simple repo for UTL discussions. 

Uses the following link from Microsfot docs extensively.

[C Sharp Docs](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/) 

This repo contains some basic stuff suitable to a introduction overview to c sharp

1. General Structure of a C program. 

2. Functions - Calling, sending a parameter, getting a return value

3. Simple class with default constructor, custom constructor that takes parameters.

3. Simple class that displays the values

4. Collections and using essential LINQ and lambda to work with this collection


---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 